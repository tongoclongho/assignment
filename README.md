# assignment
1. Build:

mvn clean install

2. Run:

mvn spring-boot:run

3. APIs:

3.1 Get closing price of a ticker in specific dates:

GET: http://localhost:8080/api/v2/{tickerSymbol}/closePrice?startDate={yyyy-MM-dd}&endDate={yyyy-MM-dd}

Response:

SUCCESS:

{
    "Prices": {
        "Ticker": {tickerSymbol},
        "DateClose": [
            {
                "Date": {yyyy-MM-dd},
                "Price": {double}
            },
            {
                "Date": {yyyy-MM-dd},
                "Price": {double}
            }
        ]
    }
}

FAILED:

{
    "Error": {
        "code": {errorCode},
        "message": {errorMessage}
    }
}

3.2 Calculate 200 days moving average of a specific ticker

GET: http://localhost:8080/api/v2/{tickerSymbol}/200dma?startDate={yyyy-MM-dd}

Response

SUCCESS:

{
    "200dma": {
        "Ticker": {tickerSymbol},
        "Avg": {double}
    }
}

FAILED:

{
    "Error": {
        "code": {errorCode},
        "message": {errorMessage}
    }
}

3.3 Calculate 200 days moving average of list of tickers

POST: http://localhost:8080/api/v2/200dma?startDate={yyyy-MM-dd}

Request body: [{tickerSymbol1}, {tickerSymbol2}]

Response:

SUCCESS:

{
    "MultiTicker200dma": {
        "200dma": [            
            {
                "Ticker": {tickerSymbol1},
                "Avg": {double}
            },
            {
                "Ticker": {tickerSymbol2},
                "Avg": {double}
            }
        ]
    }
}

FAILED:

{
    "Error": {
        "code": {errorCode},
        "message": {errorMessage}
    }
}
