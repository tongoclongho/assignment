package com.sentifi.stock.controller;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import com.sentifi.stock.domain.ClosePriceRequest;
import com.sentifi.stock.domain.StockResponse;
import com.sentifi.stock.domain.Ticker200dmaAsyncRequest;
import com.sentifi.stock.domain.Ticker200dmaRequest;
import com.sentifi.stock.service.ProxyService;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

@Slf4j
@RestController
@RequestMapping(value = "")
public class StockController {
	
	@Autowired
	private ProxyService proxyService;
    
    @RequestMapping(value = "/{tickerSymbol}/closePrice", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getTickerClosePrice(@PathVariable String tickerSymbol,
    											@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, 
    											@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate){
        log.info("API for getting closing prices of ticker {}", tickerSymbol);
        ClosePriceRequest req = new ClosePriceRequest(tickerSymbol, startDate, endDate);
        return ResponseEntity.ok(proxyService.execute(req));
    }

	@RequestMapping(value = "/{tickerSymbol}/200dma", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> calculateTicker200dma(@PathVariable String tickerSymbol, 
											@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate){
	    log.info("API for calculating 200 days moving average");
	    Ticker200dmaRequest req = new Ticker200dmaRequest(tickerSymbol, startDate);
	    return ResponseEntity.ok(proxyService.execute(req));
	}
	
	@RequestMapping(value = "/200dma", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public DeferredResult<ResponseEntity<StockResponse>> calculateTicker200dmav2(@RequestBody List<String> tickers, 
											@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate){
		log.info("API for calculating 200 days moving average");
	    
	    DeferredResult<ResponseEntity<StockResponse>> result = new DeferredResult<>();
	    
	    Ticker200dmaAsyncRequest req = new Ticker200dmaAsyncRequest(tickers, startDate);
	    // separate new thread for a possible long process
	    new Thread(() -> {result.setResult(ResponseEntity.ok(proxyService.execute(req)));}).start();

        return result;
	}
	
	@RequestMapping(value = "/async-demo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public DeferredResult<ResponseEntity<?>> handleReqDefResult(Model model) {
	    log.info("Received async-deferredresult request");
	    DeferredResult<ResponseEntity<?>> output = new DeferredResult<>();
	     
	    ForkJoinPool.commonPool().submit(() -> {
	        log.info("Processing in separate thread");
	        try {
	            Thread.sleep(6000);
	        } catch (InterruptedException e) {
	        }
	        output.setResult(ResponseEntity.ok("ok"));
	    });
	     
	    log.info("servlet thread freed");
	    return output;
	}
}
