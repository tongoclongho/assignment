package com.sentifi.stock.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@JsonRootName(value = "Prices")
public class ClosePrice extends StockResponse {
	
	@JsonProperty("Ticker")
	private String ticker;
	
	@JsonProperty("DateClose")
	private List<DateClose> dateClose;
}
