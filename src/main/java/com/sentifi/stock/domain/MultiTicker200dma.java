package com.sentifi.stock.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MultiTicker200dma extends StockResponse {
	@JsonProperty("200dma")
    private List<Ticker200dma> responses;
}
