package com.sentifi.stock.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Builder
@JsonRootName(value = "200dma")
@JsonInclude(NON_NULL)
public class Ticker200dma extends StockResponse {

	@JsonProperty("Ticker")
	private String ticker;
	
	@JsonProperty("Avg")
	private Double averagePrice;
	
	@JsonProperty("Dates")
	private LocalDate[] dates;
	
	@JsonProperty("Error")
	private String message;
	
	
	public Ticker200dma(String ticker, Double averagePrice) {
		this.ticker = ticker;
		this.averagePrice = averagePrice;
	}

	public Ticker200dma(String ticker, String message) {
		this.ticker = ticker;
		this.message = message;
	}

	public Ticker200dma(String ticker, Double averagePrice, LocalDate[] dates) {
		this.ticker = ticker;
		this.averagePrice = averagePrice;
		this.dates = dates;
	}
}
