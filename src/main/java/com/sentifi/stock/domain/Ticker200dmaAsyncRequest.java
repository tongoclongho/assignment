package com.sentifi.stock.domain;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class Ticker200dmaAsyncRequest extends StockRequest {
	
	private List<String> tickers;
	private LocalDate startDate;
}
