package com.sentifi.stock.domain;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Ticker200dmaRequest extends StockRequest {

	private String ticker;
	private LocalDate startDate;
}
