package com.sentifi.stock.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
@Slf4j
public class ErrorHandler extends BaseExceptionHandler {
    public ErrorHandler() {
        super(log);

        registerMapping(TickerNotFoundException.class, "TICKER_NOT_FOUND", "Ticker not found", HttpStatus.NOT_FOUND);
        registerMapping(InvalidRangeDateException.class, "INVALID_DATE", "Range date is invalid", HttpStatus.BAD_REQUEST);
        registerMapping(QuandlServiceCallException.class, "QUANDL_SERVICE_ERROR", "Quandl service got error", HttpStatus.BAD_GATEWAY);

    }

}
