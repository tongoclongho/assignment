package com.sentifi.stock.exception;

public class InvalidRangeDateException extends RuntimeException {
	
	public InvalidRangeDateException(String message) {
		super(message);
	}
	
	public InvalidRangeDateException() {
		super();
	}

}
