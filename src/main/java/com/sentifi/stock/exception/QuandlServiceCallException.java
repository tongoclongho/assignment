package com.sentifi.stock.exception;

public class QuandlServiceCallException extends RuntimeException {
	public QuandlServiceCallException(String message) {
		super(message);
	}
	
	public QuandlServiceCallException() {
		super();
	}
}
