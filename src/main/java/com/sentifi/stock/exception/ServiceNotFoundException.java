package com.sentifi.stock.exception;

public class ServiceNotFoundException extends RuntimeException {

	public ServiceNotFoundException(String message) {
		super(message);
	}
	
	public ServiceNotFoundException() {
		super();
	}
}
