package com.sentifi.stock.exception;

public class TickerNotFoundException extends RuntimeException {

	public TickerNotFoundException(String message) {
		super(message);
	}

	public TickerNotFoundException() {
		super();
	}

}
