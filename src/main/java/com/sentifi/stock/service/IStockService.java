package com.sentifi.stock.service;

import com.sentifi.stock.domain.ClosePriceRequest;
import com.sentifi.stock.domain.StockResponse;
import com.sentifi.stock.domain.Ticker200dmaAsyncRequest;
import com.sentifi.stock.domain.Ticker200dmaRequest;

public interface IStockService {
	public StockResponse getClosingPrices(ClosePriceRequest request);
	
	public StockResponse calculateTicker200dma(Ticker200dmaRequest request);
	
	public StockResponse calculateMultiTickers200dma(Ticker200dmaAsyncRequest request);
}
