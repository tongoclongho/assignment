package com.sentifi.stock.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sentifi.stock.domain.ClosePriceRequest;
import com.sentifi.stock.domain.StockRequest;
import com.sentifi.stock.domain.StockResponse;
import com.sentifi.stock.domain.Ticker200dmaAsyncRequest;
import com.sentifi.stock.domain.Ticker200dmaRequest;
import com.sentifi.stock.exception.ServiceNotFoundException;

import lombok.extern.slf4j.Slf4j;

/**
 * Class is used to route API to appropriate service based on name of request object 
 * 
 */
@Slf4j
@Service
public class ProxyService {
	
	@Autowired
	private IStockService stockService;
	
	public StockResponse execute(StockRequest request) {
		final String clazzName = request.getClass().getSimpleName();
		switch (clazzName) {
		case "ClosePriceRequest":
			log.info("Start service getClosingPrices.");
			return stockService.getClosingPrices((ClosePriceRequest) request);
		
		case "Ticker200dmaRequest":
			log.info("Start calculate Ticker 200 days moving average");
			return stockService.calculateTicker200dma((Ticker200dmaRequest) request);
			
		case "Ticker200dmaAsyncRequest":
			log.info("Start calculate list of Tickers 200 days moving average");
			return stockService.calculateMultiTickers200dma((Ticker200dmaAsyncRequest) request);
			
		default:
			throw new ServiceNotFoundException(String.format("No appropriate service is identified with service name %s", clazzName));
		}
	}
}
