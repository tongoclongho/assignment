package com.sentifi.stock.service.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sentifi.stock.domain.ClosePrice;
import com.sentifi.stock.domain.ClosePriceRequest;
import com.sentifi.stock.domain.DateClose;
import com.sentifi.stock.domain.MultiTicker200dma;
import com.sentifi.stock.domain.StockResponse;
import com.sentifi.stock.domain.Ticker200dma;
import com.sentifi.stock.domain.Ticker200dmaAsyncRequest;
import com.sentifi.stock.domain.Ticker200dmaRequest;
import com.sentifi.stock.exception.InvalidRangeDateException;
import com.sentifi.stock.exception.QuandlServiceCallException;
import com.sentifi.stock.exception.TickerNotFoundException;
import com.sentifi.stock.service.IStockService;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Slf4j
@Service
@CacheConfig(cacheNames={"stockCache"}) 
public class StockServiceImpl implements IStockService {

	private static final int AVERAGE_MOVING_DAYS = 200;
	
	private final OkHttpClient HTTPCLIENT = new OkHttpClient();
	
	private final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Override
	public StockResponse getClosingPrices(ClosePriceRequest request) {
		validateTickerSymbol(request.getTicker());
		validateRangeDate(request.getStartDate(), request.getEndDate());

		try {
			List<DateClose> dateCloses = getDateClose(request.getTicker(), request.getStartDate(), request.getEndDate());
			return new ClosePrice(request.getTicker(), dateCloses);
		} catch (QuandlServiceCallException e) {
			throw new TickerNotFoundException(e.getMessage());
		}
	}
	
	@Override
	public StockResponse calculateTicker200dma(Ticker200dmaRequest request) {
		String ticker = request.getTicker();
		LocalDate startDate = request.getStartDate();
		LocalDate endDate = startDate.plusDays(AVERAGE_MOVING_DAYS);

		validateTickerSymbol(ticker);
		validateRangeDate(startDate, endDate);

		List<DateClose> dateCloses = getDateClose(ticker, startDate, endDate);
		if (dateCloses.isEmpty()) {
			String possibleDate = getPossibleDate200mda(ticker);
			throw new TickerNotFoundException(String.format("No data for the given date. Please try this one: %s", possibleDate));
		}

		double avg = average200DaysMoving(dateCloses).getAsDouble();
		return Ticker200dma.builder()
						   .ticker(ticker)
						   .averagePrice(Double.valueOf(avg))
						   .build();
	}
	
	@Override
	public StockResponse calculateMultiTickers200dma(Ticker200dmaAsyncRequest request) {
		log.info("Started task with {} tickers", request.getTickers().size());
		LocalDate startDate = request.getStartDate();
		LocalDate endDate = startDate.plusDays(AVERAGE_MOVING_DAYS);
		
		final Set<String> tickers = new TreeSet<>(request.getTickers()); // ensure doesnt process duplicated ticker
		final List<Ticker200dma> responses = new ArrayList<>();
		final ConcurrentMap<String, AtomicReference<String>> waitingPool = new ConcurrentHashMap<>();
		for (String ticker : tickers) {
			try {
				validateTickerSymbol(ticker);
			} catch(TickerNotFoundException e) {
				responses.add(new Ticker200dma(ticker, e.getMessage()));
				continue;
			}
			
			final AtomicReference<String> apiResult = new AtomicReference<String>();
			
			getDateClose(ticker, startDate, endDate, apiResult);
			log.info("Push ticker {} to waiting pool", ticker);
			waitingPool.put(ticker, apiResult);
		}
		
		while (true) {
			Iterator<Map.Entry<String, AtomicReference<String>>> iterator = waitingPool.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, AtomicReference<String>> entry = iterator.next();
				String json = entry.getValue().get();
				if (json != null) {
					Ticker200dma t;
					try {
						List<DateClose> rs = getTickerDateClose(json);
						if (rs.isEmpty()) {
							String possibleDate = getPossibleDate200mda(entry.getKey());
							throw new TickerNotFoundException(
									String.format("No data for the given date. Please try this one: %s", possibleDate));
						}
						t = Ticker200dma.builder().ticker(entry.getKey())
								.averagePrice(average200DaysMoving(rs).getAsDouble()).build();

					} catch (Exception e) {
						t = Ticker200dma.builder().ticker(entry.getKey()).message(e.getMessage()).build();
					}

					responses.add(t);
					log.info("Remove ticker {} from waiting pool", entry.getKey());
					iterator.remove();
				}
			}

			if (waitingPool.isEmpty()) {
				log.info("Complete process waiting pool tickers and then return.");
				break;
			}
		}
		
		return new MultiTicker200dma(responses);
	}
	
	/**
	 * Parsing JSON result for Quandl API
	 * 
	 * @param json
	 * @return
	 */
	private List<DateClose> getTickerDateClose(String json) {
		if (json != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.readTree(json);
				if (node.hasNonNull("dataset")) {
					List<DateClose> dateCloses = new ArrayList<>();
					List<List<String>> asList = mapper.readValue(node.get("dataset").get("data").toString(), new TypeReference<List<List<String>>>() {});
					asList.parallelStream().forEachOrdered(t -> {
						dateCloses.add(new DateClose(t.get(0), Double.parseDouble(t.get(1))));
					});

					return dateCloses;
				} else if (node.hasNonNull("quandl_error")) {
					String errorMessage = node.get("quandl_error").get("message").asText();
					throw new QuandlServiceCallException(errorMessage);
				}
			} catch (IOException ee) {
				log.error("Error when parsing JSON: {}", json, ee);
			}
		}

		return Collections.EMPTY_LIST;
	}

	
	private String buildUrl(String ticker, LocalDate startDate, LocalDate endDate) {
		StringBuilder baseUrl = new StringBuilder("https://www.quandl.com/api/v3/datasets/WIKI/").append(ticker).append(".json");
		
		HttpUrl.Builder route = HttpUrl.parse(baseUrl.toString()).newBuilder();
		
		route.addQueryParameter("column_index", "4");
		route.addQueryParameter("start_date", startDate.format(DATE_FORMATTER));
		route.addQueryParameter("end_date", endDate.format(DATE_FORMATTER));
		route.addQueryParameter("order", "asc");

		String url = route.build().toString();

		log.info("URL for quering data is {}", url);
		
		return url;
	}
	
	private String buildUrl(String ticker, Map<String, String> params) {
		StringBuilder baseUrl = new StringBuilder("https://www.quandl.com/api/v3/datasets/WIKI/").append(ticker).append(".json");

		HttpUrl.Builder route = HttpUrl.parse(baseUrl.toString()).newBuilder();
		for(Map.Entry<String, String> entry : params.entrySet()) {
			route.addQueryParameter(entry.getKey(), entry.getValue());
		}

		String url = route.build().toString();

		log.info("URL for quering data is {}", url);
		
		return url;
	}

	/**
	 * Ignore ticker that contains special characters, number, lowercase
	 * 
	 * @param ticker
	 */
	private void validateTickerSymbol(String ticker) {
		if (StringUtils.isEmpty(ticker) || !Pattern.matches("[A-Z]*", ticker)) {
			log.info("Invalid Ticker {}", ticker);
			throw new TickerNotFoundException(String.format("Invalid ticker: %s", ticker));
		}
	}
	
	/**
	 * Ignore startDate is after endDate, or startDate is after now
	 * 
	 * @param start
	 * @param end
	 */
	private void validateRangeDate(LocalDate start, LocalDate end) {
		if(start.isAfter(end) || start.isAfter(LocalDate.now())) {
			log.info("Invalid range date with start {} end {}", start, end);
			throw new InvalidRangeDateException(String.format("Invalid range dates [%1$s, %2$s]", start.format(DATE_FORMATTER), end.format(DATE_FORMATTER)));
		}
	}
	
	
	@Cacheable(value = "stockCache", key = "{#ticker, #startDate, #endDate}")
	private List<DateClose> getDateClose(String ticker, LocalDate startDate, LocalDate endDate){
		final AtomicReference<String> result = new AtomicReference<String>();
		callQuandlServie(buildUrl(ticker, startDate, endDate), result);
		while(true) {
			if(result.get() != null) {
				break;
			}
		}
		
		return getTickerDateClose(result.get());
	}
	
	@Cacheable(value = "stockCache", key = "{#ticker, #startDate, #endDate}")
	private List<DateClose> getDateClose(String ticker, LocalDate startDate, LocalDate endDate, final AtomicReference<String> result){
		callQuandlServie(buildUrl(ticker, startDate, endDate), result);
		return getTickerDateClose(result.get());
	}
	
	private String callQuandlServie(String url, final AtomicReference<String> result) {
		log.info("Quandl service calling by URL {}", url);
		Request httpReq = new Request.Builder().url(url).build();
		HTTPCLIENT.newCall(httpReq).enqueue(new Callback() {
			@Override
			public void onResponse(Call call, Response response) throws IOException {
				result.set(response.body().string());
			}

			@Override
			public void onFailure(Call call, IOException e) {
				log.error("Error when calling Quandl service {}", url, e);
				result.set(e.getMessage());
			}
		});

//		while (true) {
//			if (result.get() != null) {
//				log.info("Complete request and return response to API client");
//				break;
//			}
//		}

		return result.get();
	}
	
	/**
	 * 200-day moving average by taking the average of closing prices over the last 200 days
	 * 
	 * @param dateCloses
	 * @return
	 */
	private OptionalDouble average200DaysMoving(List<DateClose> dateCloses) {
		return dateCloses.parallelStream().mapToDouble(DateClose::getClosePrice).average();
	}
	
	private String getPossibleDate200mda(String ticker) {
		Map<String, String> params = new LinkedHashMap<>();
		params.put("column_index", "4");
		params.put("order", "asc");
		params.put("rows", "1");

		Request httpReq = new Request.Builder().url(buildUrl(ticker, params)).build();

		try {
			Response response = HTTPCLIENT.newCall(httpReq).execute();
			List<DateClose> dateCloses = getTickerDateClose(response.body().string());
			if (!dateCloses.isEmpty()) {
				return LocalDate.parse(dateCloses.get(0).getDateClose()).minusDays(AVERAGE_MOVING_DAYS).format(DATE_FORMATTER);
			}
		} catch (Exception e) {
			log.error("Error during call Quandl API", e);
		}

		return LocalDate.now().minusDays(AVERAGE_MOVING_DAYS).format(DATE_FORMATTER);
	}
	
}
