package com.sentifi.stock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import com.sentifi.stock.domain.ClosePrice;
import com.sentifi.stock.domain.ClosePriceRequest;
import com.sentifi.stock.domain.DateClose;
import com.sentifi.stock.domain.MultiTicker200dma;
import com.sentifi.stock.domain.Ticker200dma;
import com.sentifi.stock.domain.Ticker200dmaAsyncRequest;
import com.sentifi.stock.domain.Ticker200dmaRequest;
import com.sentifi.stock.exception.InvalidRangeDateException;
import com.sentifi.stock.exception.TickerNotFoundException;
import com.sentifi.stock.service.ProxyService;
import com.sentifi.stock.service.impl.StockServiceImpl;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StockPriceControllerTest {
	
	@Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProxyService proxyService;
    
    @MockBean
    private StockServiceImpl stockService;

	@Test
	public void testGetClosePriceOK() throws Exception {
		List<DateClose> list = new ArrayList<>();
		ClosePrice result = new ClosePrice("GE", list);
		ClosePriceRequest req = new ClosePriceRequest("GE", LocalDate.parse("2017-03-27"), LocalDate.parse("2018-03-27"));
		given(proxyService.execute(req)).willReturn(result);
		mockMvc.perform(get("/{tickerSymbol}/closePrice?startDate=2017-03-27&endDate=2018-03-27", "GE").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andDo(print()).andExpect(jsonPath("$.Prices.Ticker").value(result.getTicker()));
	}
	
	@Test
	public void testGetClosePriceInvalidTickerThrow404() throws Exception {
		ClosePriceRequest req = new ClosePriceRequest("G2323E", LocalDate.parse("2017-03-27"), LocalDate.parse("2018-03-27"));
		given(proxyService.execute(req)).willThrow(TickerNotFoundException.class);
		mockMvc.perform(get("/{tickerSymbol}/closePrice?startDate=2017-03-27&endDate=2018-03-27", "G2323E").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isNotFound()).andDo(print()).andExpect(jsonPath("$.Error.code").value("TICKER_NOT_FOUND"));
	}
	
	@Test
	public void testGetClosePriceInvalidRangeDateThrow400() throws Exception {
		ClosePriceRequest req = new ClosePriceRequest("GE", LocalDate.parse("2019-03-27"), LocalDate.parse("2018-03-27"));
		given(proxyService.execute(req)).willThrow(InvalidRangeDateException.class);
		mockMvc.perform(get("/{tickerSymbol}/closePrice?startDate=2019-03-27&endDate=2018-03-27", "GE").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest()).andDo(print()).andExpect(jsonPath("$.Error.code").value("INVALID_DATE"));
	}
    
	@Test
	public void testCalculate200dmaOK() throws Exception {
		Ticker200dma result = Ticker200dma.builder().ticker("GE").averagePrice(10.0).build();
		Ticker200dmaRequest req = new Ticker200dmaRequest("GE", LocalDate.parse("2017-03-27"));
		given(proxyService.execute(req)).willReturn(result);
		mockMvc.perform(get("/{tickerSymbol}/200dma?startDate=2017-03-27", "GE").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andDo(print()).andExpect(jsonPath("$.200dma.Ticker").value(result.getTicker()));
	}
	
	@Test
	public void testCalculate200dmaInvalidTickerThrow404() throws Exception {
		Ticker200dmaRequest req = new Ticker200dmaRequest("G23E", LocalDate.parse("2017-03-27"));
		given(proxyService.execute(req)).willThrow(TickerNotFoundException.class);
		mockMvc.perform(get("/{tickerSymbol}/200dma?startDate=2017-03-27", "G23E").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isNotFound()).andDo(print()).andExpect(jsonPath("$.Error.code").value("TICKER_NOT_FOUND"));
	}
	
	@Test
	public void testGetClosePriceInvalidRangDateThrow400() throws Exception {
		Ticker200dmaRequest req = new Ticker200dmaRequest("GE", LocalDate.parse("2019-03-27"));
		given(proxyService.execute(req)).willThrow(InvalidRangeDateException.class);
		mockMvc.perform(get("/{tickerSymbol}/200dma?startDate=2019-03-27", "GE").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest()).andDo(print()).andExpect(jsonPath("$.Error.code").value("INVALID_DATE"));
	}
    
	@Test
	public void testCalculateMultiTicker200dmaOK() throws Exception {
		Ticker200dma ticker200dma = Ticker200dma.builder()
												.ticker("GE")
												.averagePrice(10.0)
												.build();
		
		MultiTicker200dma result = MultiTicker200dma.builder()
													.responses(Stream.of(ticker200dma).collect(Collectors.toList()))
													.build();
		
		Ticker200dmaAsyncRequest req = new Ticker200dmaAsyncRequest(Stream.of("GE").collect(Collectors.toList()), LocalDate.parse("2017-03-27"));
		given(proxyService.execute(req)).willReturn(result);
		
		MvcResult mvcResult = mockMvc.perform(post("/200dma?startDate=2017-03-27").contentType("application/json").content("[\"GE\"]"))
				 .andExpect(request().asyncStarted())
				 .andReturn();
		
		mockMvc.perform(asyncDispatch(mvcResult)).andExpect(status().isOk()).andDo(print());
	}
	
}
